import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Home from './pages/Home';
import Orders from './pages/Orders';
import AppNavBar from './components/AppNavBar';
import Login from './pages/Login';
import Register from './pages/Register';
import Contact from './pages/Contact';
import { UserProvider } from './userContext';
import { OrderProvider } from './orderContext';
import {useState, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import Footer from './components/Footer';
import Dashboard from './pages/Dashboard';
import DashboardAdmin from './pages/Dashboard-admin';
import Logout from './pages/Logout';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    publisher: null,
    name: null
  });

  const [myProducts, setMyProducts] = useState([]);

  const [orders, setOrders] = useState([]);

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <OrderProvider value={{orders, setOrders}}>
      <Router>
      <AppNavBar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Orders" element={<Orders />} />
            <Route path="/Contact" element={<Contact />} />
            <Route path="/Dashboard" element={<Dashboard />} />
            <Route path="/Dashboard/admin" element={<DashboardAdmin />} />
            <Route path="/Logout" element={<Logout />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/Signup" element={<Register />} />
          </Routes>
        </Container>
        <Footer/>
      </Router>
      </OrderProvider>
    </UserProvider>
  );
}

export default App;