import './pages.css'
import Card from '../components/Cards';
import Carousel from '../components/Carousel';
import CardCarousel from '../components/CardCarousel';
import DisplayCard from '../components/DisplayCard';


function UncontrolledExample() {

  

  return (
      <div> 
        <Carousel/>
        <h1 style={{textAlign: "center", marginTop: "2rem", marginBottom: "2rem", color: "black"}}>Featured</h1>
        <CardCarousel />
      </div>
  );
}

export default UncontrolledExample;