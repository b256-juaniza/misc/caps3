import '../components/component.css'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import swal from 'sweetalert2';
import UserContext from '../userContext';

function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const {user, setUser} = useContext(UserContext);

  const navigate = useNavigate();

    function login(e) {
      e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
          method: "POST",
          headers: {
            'Content-Type' : 'Application/json'
          },
          body: JSON.stringify({
            email: email, 
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {

          console.log(data);

          if(typeof data.access !== "undefined") {
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

            swal.fire({
              title: "Authentication Successful",
              icon: "success",
              text: "Login Successfully!"
            })

          navigate("/Dashboard")

          } else {
            swal.fire({
              title: "Wrong Email or Password!",
              icon: "error",
              text: "Check your Email or Password and try again!"
            })
          }
        })

        const retrieveUserDetails = (token) => {

          fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${token}`
            }
          })
          .then(res => res.json())
          .then(data => {
            console.log(data);
            setUser({
              id: data._id,
              name: data.name,
              email: data.email,
              publisher: data.publisher,
              isAdmin: data.isAdmin
            })
          })
        }
      }

  return (
    (user.id !== undefined && user.id !== null) ?
        (user.publisher === true) ?
        <Navigate to="/Dashboard/admin" />
      :
        <Navigate to="/Dashboard" />
      :
      <Container>
        <div style={{marginTop: "0vh", marginBottom: "15.5vh", marginTop: "5vh"}} className="d-flex flex-row justify-content-center">
          <Form onSubmit={e => login(e)} style={{background: "peachpuff", width: "30vw", border: "2px solid black", borderRadius: "10px"}}
             className="col-3 p-5 pt-3 m-5 d-flex flex-column align-items-center">
             <Form.Group className="mb-3 mt-5" controlId="formBasicEmail2">
              <Form.Label className="fw-bold">Email</Form.Label>
              <Form.Control style={{width: "20vw"}} type="email" placeholder="" value={email} onChange={e => setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail3">
              <Form.Label className="fw-bold">Password</Form.Label>
              <Form.Control style={{width: "20vw"}} type="password" placeholder="" value={password} onChange={e => setPassword(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </div>
      </Container>
  );
}

export default Login;