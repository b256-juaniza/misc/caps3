import { Container } from 'react-bootstrap'; 
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useState, useEffect, useContext, useParams  } from 'react';
import UserContext from '../userContext';
import DashboardCard from '../components/DashboardCard';

export default function Dashboard() {

	const [books, setBooks] = useState([]);
	const {user, setUser} = useContext(UserContext);

	console.log(books)
	useEffect(() => {	

		/*const element = document.getElementById("productsId");
	    if (element) {
	    	element.scrollIntoView({ behavior: 'smooth' });
		}*/

		fetch(`${process.env.REACT_APP_API_URL}/books/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setBooks(data.map(prod => {
				console.log(prod);
				return(
					<DashboardCard key={prod._id} prods={prod} />
				)
				}))
			}) 
		})

	return (
		<Container className="opacity p-5 mb-5" style={{height: "100%"}}>
		    {books}
		</Container>
	)
}