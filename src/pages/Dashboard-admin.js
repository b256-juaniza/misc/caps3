import '../components/component.css'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import BookDetails from '../components/BookDetails';
import Table from 'react-bootstrap/Table';

export default function Dashboard() {

		const [name, setName] = useState("");
		const [genre, setGenre] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState();
		const [image, setImage] = useState();
		const navigate = useNavigate();
		const { user, setUser } = useContext(UserContext);
		const [text, setText] = useState("");
		const [ isActive, setActive] = useState(true);
		const [books, setBooks] = useState([]);

		useEffect(() => {
			if (user.id === undefined || user.id === null) {
				navigate("/Login");
			}
			fetch(`${process.env.REACT_APP_API_URL}/books/all`)
			.then(res => res.json())
			.then(data => {
				setBooks(data.map(prod => {
					return(
						<>
							<BookDetails key={prod._id} prods={ prod } />
						</> 
					)
				}))
			})
		})

	function edit(prod) {
			fetch(`${process.env.REACT_APP_API_URL}/update/${prod}`, {
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			})
		}

	function create(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/books/create`, {
			method: "POST",
			headers: {
				'Content-Type' : 'Application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				image: image,
				name: name,
				genre: genre,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			Swal.fire({
				title: "Created Successfully",
				icon: "success",
				text: "The Product is now Availabe"
			})
			setName("");
			setDescription("");
			setPrice('');
			setImage('');
			setGenre('');
		})
	}

	return (
			(user.publisher === true) ?
			<div className="">
				<div className="d-flex flex-column justify-content-center" style={{height: "", width: "100%", overflow: "hidden"}}>
					<h1 style={{textAlign: "center"}} className="pt-5">Publish Book</h1>
					<Container style={{marginLeft: "18vw"}} className="d-flex flex-row p-5">
					    <div style={{height: "100%"}} className="col">
					    	<Form id="myform1" onSubmit={e => create(e)} style={{background: "rgba(0, 0, 0, 0.2)"}}
							     className="p-5 d-flex m-3 flex-column align-items-center">
							     <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail1">
							        <Form.Label>Cover Image: <br/></Form.Label>
							        <Form.Control type="text" placeholder="" value={image} onChange={e => setImage(e.target.value)}/>
							      </Form.Group>
							      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail1">
							        <Form.Label>Book Title: </Form.Label>
							        <Form.Control type="text" placeholder={text} value={name} onChange={e => setName(e.target.value)}/>
							      </Form.Group>
							      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail1">
							        <Form.Label>Genre: </Form.Label>
							        <Form.Control type="text" placeholder="" value={genre} onChange={e => setGenre(e.target.value)}/>
							      </Form.Group>
							      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail2">
							        <Form.Label>Description: </Form.Label>
							        <Form.Control as="textarea" rows="6" placeholder="" value={description} onChange={e => setDescription(e.target.value)}/>
							      </Form.Group>
							      <Form.Group style={{width: "300px"}} className="mb-3" controlId="formBasicPassword">
							        <Form.Label>price: </Form.Label>
							        <Form.Control type="number" placeholder="" value={price} onChange={e => setPrice(e.target.value)}/>
							      </Form.Group>
							      <Button variant="primary" type="submit">
							        PUBLISH
							      </Button>
							</Form>
					    </div>
					</Container>
			    </div>
			    <div>
			    	<Table style={{border: "2px solid black", backgroundColor: "peachpuff"}} striped bordered hover id="myTable" className="text-center">
						<thead style={{background: "rgba(0,0,0,0.3)"}}>
							<tr>
					          <th>Title</th>
					          <th>Genre</th>
					          <th>Description</th>
					          <th>Price</th>
					          <th>Book Details</th>
					          <th>Unpublish</th>
					        </tr>
					    </thead>
						<tbody style={{width: "50px"}}>
							{ books }
						</tbody>
					</Table>
			    </div>
		    </div>
		    :
		    <Navigate to="/Dashboard "/>
	)
}