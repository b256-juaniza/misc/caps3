import '../components/component.css'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

function Register() {
  return (
    <Container>
      <div style={{marginTop: "0vh", marginBottom: "3vh"}} className="d-flex text-center flex-row justify-content-center">
		<Form style={{background: "peachpuff", width: "30vw", borderRadius: "10px", border: "1px solid black"}}
			 className="col-3 p-5 m-5 d-flex flex-column align-items-center">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Name:</Form.Label>
            <Form.Control style={{width: "20vw"}} type="text" placeholder="Enter Name" />
          </Form.Group>
           <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Contact Number</Form.Label>
            <Form.Control style={{width: "20vw"}} type="number" placeholder="" />
          </Form.Group>
           <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control style={{width: "20vw"}} type="email" placeholder="Enter Email" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Message</Form.Label>
            <Form.Control style={{height: "20vh", width: "20vw", resize: "none"}} as="textarea" aria-label="Enter your Concerns"/>
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    </Container>
  );
}

export default Register;