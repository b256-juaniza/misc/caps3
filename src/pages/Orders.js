import { useState, useEffect, useContext } from 'react';
// import CourseData from '../data/CourseData';
import UserOrders from '../components/UserOrders';
import Table from 'react-bootstrap/Table';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';

export default function Orders() {

	const [orders, setOrders] = useState([]);
	const [products, setProducts] = useState([]);
	const [func, setFunct] = useState();
	const {user, setUser} = useContext(UserContext);
	const { name } = user;

	useEffect(() => {
		if (user.id !== null && user.id !== undefined) {
				<Navigate to="/Logout" />
      }

		fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
			method: "POST",
			headers: { 
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data.Orders.map(res => {
				return(
					 <UserOrders key={res._id} orders={res} /> 
				)
			}))
		})
	})

	return (
		<div className="mt-5 ms-5" style={{height: "100vh"}}>
			<Table style={{background: "peachpuff"}} striped bordered hover id="myTable" className="text-center">
			<thead className="opacity">
				<tr className="opacity">
		          <th className="opacity">Book</th>
		          <th className="opacity">Time of Purchase</th>
		          <th className="opacity">Quantity</th>
		          <th className="opacity">Price</th>
		          <th className="opacity">Total Amount</th>
		          <th className="opacity">Status</th>
		          <th className="opacity">Product Details</th>
		        </tr>
		    </thead>
			<tbody style={{width: "50px", background: "rgba(0,0,0,0.1)"}}>
				{orders}
			</tbody>
		</Table>
		</div>
	)
}