import { codeSquare } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';

export default function Footer() {
	return (
		<>
			<div style={{background: "light-grey", borderTop: "solid 2px black", bottom: "0"}} className="container-fluid text-left ml-0 pr-0">
			<div className="row opacity">
				<div className="text-left col">
					<div className="ml-0 pr-0 d-flex flex-column flex-xl-row flex-lg-row">
			  			<div className="col col-xl-3 col-lg-3 col-md-5 ml-md-5 col-xl-0 col-lg-0 mr-5 ml-0 col-sm-12 mt-2 mb-2">
			  				<div>
								<h6 className="text-9 text-left">Contacts</h6>
							</div>
							<div className="">
								<div className="col-3">
									<div className="d-flex">
										<p className="text-8 m-0 d-flex align-items-center">Facebook: </p>
										<a style={{textDecoration: "none"}} href="facebook.com" className="text-7 span-1 pl-2 m-0 d-flex align-items-center ms-2"></a>
									</div>
								</div>
								<div className="col-3">
									<div className="d-flex">
										<p className="text-8 m-0 d-flex align-items-center">Gmail: </p>
										<p className="text-7 span-1 pl-2 m-0 d-flex align-items-center ms-2"></p>
									</div>
								</div>
								<div className="col-3">
									<div id="phone" className="d-flex">
										<p className="text-8 m-0 d-flex align-items-center">Phone: </p>
										<p className="text-7 span-1 pl-2 m-0 d-flex align-items-center ms-2"></p>
									</div>
								</div>
							</div>
			  			</div>
			  			<div className="col col-xl-3 col-lg-3 col-md-5 ml-md-5 col-xl-0 col-lg-0 mr-5 ml-0 col-sm-12 mt-2 mb-2">
			  				<div>
								<h6 className="text-9 text-left">About</h6>
							</div>
							<div className="col-3">
								<div id="phone" className="d-flex flex-row">
									<p className="text-8 m-0 d-flex align-items-center">Website </p>
								</div>
							</div>
							<div className="col-3">
								<div id="phone" className="d-flex flex-row">
									<p className="text-8 m-0 d-flex align-items-center">Me</p>
								</div>
							</div>
							<div className="col-3">
								<div className="d-flex flex-row">
									<p className="text-8 m-0 d-flex align-items-center">Contact</p>
								</div>
							</div>
			  			</div>
			  			<div className="col mt-1 col-xl-3 col-lg-3 col-md-3  mr-5 ml-0 col-sm-12 ">
			  				<div>
								<h6 className="text-9 text-left">Services</h6>
							</div>
							<div className="col-3">
								<div id="phone" className="d-flex flex-row justify-content-between">
									<p className="text-8 m-0 d-flex align-items-center">Web</p> <span className="trans">-</span>
									<p className="text-8 m-0 d-flex align-items-center">Development</p>
								</div>
							</div>
							<div className="col-3">
								<div id="phone" className="d-flex flex-row">
									<p className="text-8 m-0 d-flex align-items-center">Software</p>
									<span className="trans">-</span>
									<p className="text-8 m-0 d-flex align-items-center">Development</p>
								</div>
							</div>
			  			</div>
		  			</div>
				</div>
			</div>
			<div className="row" style={{background: "rgba(0,0,0,0.2)", color: "azure"}}>
				<div className="d-flex justify-content-center">
					<p className="pr-1 d-flex m-0 align-items-center">&#169;</p>
					<p className="pl-1 pr-1 m-0 d-flex align-items-center">| 2023</p>
					<p className="pl-1 d-flex m-0 align-items-center">| LynnD</p>
				</div>
			</div>
		</div>
	</>
	)
}