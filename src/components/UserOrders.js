import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Carousel from 'react-bootstrap/Carousel';
import './component.css';
import { Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import UserContext from '../userContext';
import OrderContext from '../orderContext';

export default function ProductDetails({orders}) {
	// const {orders, setOrders} = useContext(OrderContext);

	const { orderId, Product, description, quantity, price, productsId, purchaseOn, Total } = orders;
	const {user, setUser} = useContext(UserContext);
	const [prodDesc, setDesc] = useState("");
	const [prodPrice, setPrice] = useState();

	const [image, setImage] = useState("");

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const [ prod, setProd ] = useState(0);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/books/check`, {
			method: "POST",
			headers: { 
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productsId: productsId
			})
		})
		.then(res => res.json())
		.then(data => {
			setImage(data.image)
			setDesc(data.description)
		})
	})

	function sets(e) {
		setShow(true);
		setProd(e);
	}

	return (
	<>
		<Modal id="thisModal" className="text-center" show={show} onHide={handleClose}>
			    <Modal.Header style={{textAlign: "center", height: "4rem"}} className="d-flex flex-column text-center align-items-center" closeButton>
			      <Modal.Title className="">{Product}</Modal.Title>
			    </Modal.Header>
			    <Modal.Body className="" style={{background: "peachpuff"}}>
					<Card style={{background: "rgba(0,0,0,0.1)", width: '100%', height: "500px", border: "solid 1px black"}} className="d-flex flex-column align-items-center pt-3">
				      <Card.Img variant="top" style={{width: '75%', height: "300px"}} src={image} />
				      <Card.Body>
				        <Card.Title>{Product}</Card.Title>
				        <div id="modalDesc" style={{background: "rgba(0,0,0,0.2)", height: "130px", textAlign: "justify", overflowY: "scroll", textOverflow: "hidden"}}>
				        	<Card.Text className="ps-3 pe-3 fs-6 pt-2 pb-2" >
					          {prodDesc}
					        </Card.Text>
				        </div>
				      </Card.Body>
				    </Card>
			    </Modal.Body>
			    <Modal.Footer className="d-flex flex-row justify-content-center">
			    <Button variant="secondary" style={{border: "solid 2px black"}} className="me-3 pl-3 pr-3" onClick={handleClose}>
			        Close
			    </Button>
			    </Modal.Footer>
			</Modal>
		<tr className="">
          	<td style={{fontWeight: "bold"}}>{Product}</td>
         	<td>{purchaseOn}</td>
         	<td>{quantity}</td>
         	<td>&#8369;{price}</td>
         	<td>&#8369;{Total}</td>
         	<td style={{color: "yellow"}}>PENDING</td>
         	<td><button id="bt1" style={{ marginLeft: "20px", marginRight: "20px", padding: "5px"}} onClick={ () => sets(productsId) }>Details</button></td>
	 	</tr>
	</>
	)
}