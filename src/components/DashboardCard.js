import CardGroup from 'react-bootstrap/CardGroup';
import { Button } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card';
import { useState, useContext } from 'react';
import { Form } from 'react-bootstrap'
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

export default function DashboardCard({prods}) {

	const { name, description, price, productsId, genre, image} = prods;
	const [ quantity, setQuantity ] = useState(0);
	const [ prod, setProd ] = useState();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const { user } = useContext(UserContext);

	function set(e) {
		setShow(true);
		setProd(e);
	}

	function Order(e) {
		e.preventDefault();
			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productsId: prod,
					quantity: quantity,
				})
			})
			Swal.fire({
				title: "Ordered Successfully!",
				icon: "success",
				text: "Thank you for Purchasing!"
			})
		}

	return (
		<>
			<Modal id="thisModal" className="text-center" show={show} onHide={handleClose}>
			    <Modal.Header style={{textAlign: "center", height: "4rem"}} className="d-flex flex-column text-center align-items-center" closeButton>
			      <Modal.Title className="">{name}</Modal.Title>
			    </Modal.Header>
			    <Modal.Body className="" style={{background: "peachpuff"}}>
					<Card style={{ width: '100%', background: "rgba(0,0,0,0.1)", height: "500px", border: "solid 1px black"}} className="d-flex flex-column align-items-center pt-3">
				      <Card.Img variant="top" style={{width: '75%', height: "300px"}} src={image} />
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <div id="modalDesc" style={{height: "130px", background: "peachpuff", textAlign: "justify", overflowY: "scroll", textOverflow: "hidden"}}>
				        	<Card.Text className="ps-3 pe-3 fs-6 pt-2 pb-2" >
					          {description}
					        </Card.Text>
				        </div>
				      </Card.Body>
				    </Card>
			    </Modal.Body>
			    <Modal.Footer className="d-flex flex-row justify-content-center">
			    <Button variant="secondary" style={{border: "solid 2px black"}} className="me-3 pl-3 pr-3" onClick={handleClose}>
			        Back
			    </Button>
			   {
			   	(user.publisher !== true)  ?
			   	<>
				    <Button type="submit" form="thisForm" style={{border: "solid 2px black"}} className="pl-3 pr-3" onClick={handleClose} variant="secondary">Place Order</Button>
				    <Form className="p-0 m-0 d-flex" id="thisForm" name="thisForm" onSubmit={e => Order(e) }>
					  <Form.Group className="mt-0" style={{position: "relative", top: "-10px", left: "20px"}} controlId="formBasicEmail">
					    <Form.Label className="align-items-center p-0 m-0">Quantity</Form.Label>
					    <Form.Control style={{height: "25px"}} type="number" placeholder="" value={quantity} onChange={e => setQuantity(e.target.value)}/>
					  </Form.Group>
					</Form>
				</>
				:
					<p></p>
				}
			    </Modal.Footer>
			</Modal>
			<div id={productsId} style={{border: "2px solid black", overflow: "hidden", overflowX: "hidden", background: "peachpuff"}} className="d-flex flex-row align-items-center mb-3 justify-content-between">
				<div className="col-2">
					<img style={{height: "245px", width: "210px", border: "3px double black", inlineSize: "fit-content(20em)", objectFit: "cover", background: "rgba(0,0,0,0.5)"}} src={image} />
				</div>
				<Card.Body className="align-items-center">
					<div style={{height: "200px", boxSizing: "border-box"}} className="col ps-3 pe-1 text-center">
						<h3 className="mb-0">{name}</h3>
						<p className="p-0 m-0"><span className="fw-bold te">Genre:</span> - {genre} -</p>
						<div id="modalDesc" style={{ height: "130px", background: "peachpuff", textAlign: "justify", overflowY: "auto", textOverflow: "hidden"}}>
							<p className="pe-3 ps-3" style={{}}>{description}</p>
						</div>								
					</div>
					<div className="d-flex flex-column align-items-center text-center">
						<Button id="bt3" style={{background: "#e28743"}} onClick={() => set(productsId)}>Details</Button>
					</div>
				</Card.Body>
			</div>
		</>
	)
}