import './component.css';
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'; 
import logo from '../images/logo.png';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../userContext';
import {  CDBSidebar,  CDBSidebarContent,  CDBSidebarFooter,  CDBSidebarHeader,  CDBSidebarMenu, CDBSidebarMenuItem,} from 'cdbreact';
import { Power, EnvelopeAt, CartCheck, Grid, PersonCircle } from 'react-bootstrap-icons';

export default function AppNavBar() {

	const { user } = useContext(UserContext);
	const [admin, setAdmin] = useState("/Dashboard");

	return (
		<Navbar className="fw-bold fs-5" expand="lg">
	      <Container>
	        <Navbar.Brand className="fs-3" as={Link} to="/mySystem/"><img style={{height: "7vh", width: "12vw"}} src={logo}/></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto">
	          {
		           (user.id !== undefined && user.id !== null) ?
		          <>
		           	<Nav.Link className="link" as={NavLink} to="/">Home</Nav.Link>
		           	{
			           	(user.publisher === true) ?
			           	<>
			           		<Nav.Link className="link" as={NavLink} to="/Dashboard/admin">Publish</Nav.Link>
			           		<Nav.Link className="link" as={NavLink} to="/Dashboard">Dashboard</Nav.Link>
			           	</>
			           :
			           <>
			           	<Nav.Link className="link" as={NavLink} to="/Dashboard">Dashboard</Nav.Link>
			           	<Nav.Link className="link" as={NavLink} to="/Orders">Library</Nav.Link>
			           </>
		           	}
		            <Nav.Link className="link" as={NavLink} to="/Contact">Contact</Nav.Link>
		            <Nav.Link className="link" as={NavLink} to="/Logout">Logout</Nav.Link>
		          </>
		           :
		          <>
		          	<Nav.Link className="link" as={NavLink} to="/">Home</Nav.Link>
		            <Nav.Link className="link" as={NavLink} to="/Signup">Sign-up</Nav.Link>
		            <Nav.Link className="link" as={NavLink} to="/Login">Log-in</Nav.Link>
		          </>
		      }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}