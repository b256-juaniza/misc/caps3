import React, { Component } from 'react';
import ReactCardCarousel from 'react-card-carousel';
import './component.css';
import { useEffect, useContext, useState } from 'react';
import { Container } from 'react-bootstrap';
import {Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink, Navigate} from 'react-router-dom'; 
import UserContext from '../userContext';
import DisplayCard from './DisplayCard';
import ReactCardSlider from "react-card-slider-component";
import { useNavigate } from 'react-router-dom'

export default function CardCarousel() {

		const [books, setBooks] = useState([]);
		const {user, setUser} = useContext(UserContext);
		const navigate = useNavigate();

		const sliderClick = (id)=> {
			if (user.id !== null && user.id !== undefined) {
				navigate(`/Dashboard`);
			} else {
				navigate(`/Login`);
			}
  		}

		console.log(books)
		useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/books/all`)
			.then(res => res.json())
			.then(data => {
				console.log(data)
				setBooks(data.map(prod => {
					console.log(prod);
					return(
						{
						  image: prod.image,
					      title: prod.name,
					      description: prod.genre,
					      clickEvent: () => sliderClick(prod.productsId)
						}
					)
				}))
			})
		})

	return (
		<div style={{width: "100%"}} className="text-center">
			<ReactCardSlider className="sdd" slides={books} />
		</div>
		    
      // <ReactCardCarousel autoplay={ true } autoplay_speed={ 2500 }>
      //   { books }
      // </ReactCardCarousel>
	)	
}