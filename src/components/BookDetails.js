import Table from 'react-bootstrap/Table';
import './component.css';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

export default function ProductsTable({ prods }) {

	const { name, description, price, genre, productsId, image } = prods;
	const [prodName, setName] = useState(name);
	const [prodGenre, setGenre] = useState(genre);
	const [prodDesc, setDescription] = useState(description);
	const [prodPrice, setPrice] = useState(price);
	const [prodImage, setImage] = useState(image);
	const [ isActive, setActive ] = useState(false);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const [ prod, setProd ] = useState(0);

	const del = (prods) => {
			fetch(`${process.env.REACT_APP_API_URL}/books/delete`, {
			method: "DELETE",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productsId: prods
			})
		})
		Swal.fire({
			title: `- Deleted! -`,
			icon: "warning",
			text: `The Book ${prodName} was Removed!`
		})
	}

	function sets(e) {
		setShow(true);
		setProd(e);
	}

	function edit(e) {
		e.preventDefault();
			fetch(`${process.env.REACT_APP_API_URL}/books/update`, {
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productsId: prod,
					name: prodName,
					genre: prodGenre,
					description: prodDesc,
					price: prodPrice
				})
			})
			Swal.fire({
				title: "Updated!",
				icon: "success",
				text: `The Book ${prodName} Details is Successfully Modified!`
			})
		}

	return (
		<>
			  <Modal id="mymodal" show={show} onHide={handleClose}>
			    <Modal.Header style={{background: "lightgray"}} closeButton>
			      <Modal.Title>Edit Book</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			      <Form name="myform" id="myform" onSubmit={e => edit(e)} style={{background: "rgba(0, 0, 0, 0.2)"}}
				     className="col-3 p-5 d-flex m-3 flex-column align-items-center rounded">
				     <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail1">
				      	<Form.Control type="number" placeholder="" hidden value={prod}/>
				        <Form.Label>Cover Image: </Form.Label>
				        <Form.Control as="textarea" rows="3" placeholder="" value={prodImage} onChange={e => setImage(e.target.value)}/>
				      </Form.Group>
				      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail3">
				      	<Form.Control type="number" placeholder="" hidden value={prod}/>
				        <Form.Label>Book Title: </Form.Label>
				        <Form.Control type="text" placeholder="" value={prodName} onChange={e => setName(e.target.value)}/>
				      </Form.Group>
				      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail3">
				      	<Form.Control type="number" placeholder="" hidden value={prod}/>
				        <Form.Label>Genre: </Form.Label>
				        <Form.Control type="text" placeholder="" value={prodGenre} onChange={e => setGenre(e.target.value)}/>
				      </Form.Group>
				      <Form.Group style={{width: "550px"}} className="mb-3" controlId="formBasicEmail2">
				        <Form.Label>Description: </Form.Label>
				        <Form.Control style={{textAlign: "justify", padding: "20px"}} as="textarea" rows="6" placeholder="" value={prodDesc} onChange={e => setDescription(e.target.value)}/>
				      </Form.Group>
				      <Form.Group style={{width: "200px"}} className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>price: </Form.Label>
				        <Form.Control type="number" placeholder="" value={prodPrice} onChange={e => setPrice(e.target.value)}/>
				      </Form.Group>
					</Form>
			    </Modal.Body>
			    <Modal.Footer className="d-flex flex-row justify-content-center">
			      <Button variant="secondary" onClick={handleClose}>
			        Cancel
			      </Button>
			      <Button variant="primary" type="submit" form="myform" onClick={handleClose}>
			        Save Changes
			      </Button>
			    </Modal.Footer>
			  </Modal>
			<tr className="">
	          	<td><div style={{width: "200px"}} className="mt-4">{name}</div></td>
	          	<td><div style={{width: "200px"}} className="mt-4">{genre}</div></td>
	         	<td className="align-items-center" style={{fontSize: "0.8rem"}}><div style={{height:"100px", overflow: "hidden", textAlign: "justify"}}>{description}</div></td>
	         	<td className="" style={{paddingTop: "45px"}}>&#8369;{price}</td>
	         	<td><button id="bt1" style={{ marginLeft: "20px", marginRight: "20px", marginTop: "35px"}} className="ps-3 pe-3 fw-bold" onClick={ () => sets(productsId) } >Edit</button></td>
	         	<td><button id="bt2" style={{ marginLeft: "20px", marginRight: "20px", marginTop: "35px"}} className="ps-3 pe-3 fw-bold" onClick={ () => del(productsId) }>Remove</button></td>
		 	</tr>
		</>		   	    
	)
}