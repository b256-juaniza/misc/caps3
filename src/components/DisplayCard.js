import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import './component.css';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { Form } from 'react-bootstrap'

export default function DisplayCard({ data }) {

	const { name, description, price, productsId, image} = data;
	console.log(data);
	const {user, setUser} = useContext(UserContext);
	const [prodName, setName] = useState(name);
	const [prodDesc, setDescription] = useState(description);
	const [prodPrice, setPrice] = useState(price);
	const [prodImage, setImage] = useState(image);
	const [quantity, setQuantity] = useState(0);
	const [ isActive, setActive ] = useState(false);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const [ prod, setProd ] = useState(0);

	function sets(e) {
		setShow(true);
		setProd(e);
	}

	function Order(e) {
		e.preventDefault();
			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					userId: user.id,
					productsId: prod,
					description: prodDesc,
					quantity: quantity,
					price: prodPrice
				})
			})
			Swal.fire({
				title: "Ordered Successfully!",
				icon: "success",
				text: "Thank you for Purchasing!"
			})
		}

	return (
		<div className=" myCarousel ">
          <Card className="d-flex align-items-center pt-4" style={{ height: "400px", width: '250px', border: "solid 2px black" }}>
		      <Card.Img style={{maxHeight: "110px", width: "75%", objectFit: "fill", objectPosition: "50% 50%"}} variant="top" src={image} />
		      <Card.Body className="w-100 mt-2 opacity d-block" style={{height: "300px", overflow: "hidden"}}>
		        <Card.Title style={{fontSize: "1rem"}} className="text-center">{name}</Card.Title>
		        <Card.Text  style={{textAlign: "justify", fontSize: "0.9rem", textOverflow: "ellipsis"}}>
		          {description}
		        </Card.Text>
		      </Card.Body>
		      <div style={{height: "60px", background: "black"}} className="w-100 d-flex flex-column align-items-center">
		      	<Card.Text style={{color: "azure", fontSize: "1rem", marginBottom: "0px"}} className="fw-bold">
		          &#8369;{price}
		      	</Card.Text>
		      	 <Button style={{fontSize: "0.8rem", marginTop: "0", marginBottom: "20px"}} variant="primary" type="button" onClick={() => sets(productsId)}>Checkout</Button>
		      </div>
		    </Card>
        </div>
	)
}