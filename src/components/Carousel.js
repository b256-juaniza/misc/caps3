import './component.css';
import Carousel from 'react-bootstrap/Carousel';
import { useNavigate } from 'react-router-dom'
import img1 from '../images/cover1.png';
import img2 from '../images/cover2.png';
import img3 from '../images/cover3.png';

function UncontrolledExample() {

  const navigate = useNavigate();

  function btnRegister() {
      navigate("/Signup");
  }

  return (
      <div className="opacity pl-5 pr-5 pt-4 pb-4 margin-0">
        <Carousel className="">
          <Carousel.Item>
              <img
                style={{height: "60vh"}}
                className="w-100 img-center"
                src={img1}
                alt="First slide"
              />
          </Carousel.Item>
          <Carousel.Item>
              <img
                style={{height: "60vh"}}
                className="w-100"
                src={img2}
                alt="Second slide"
              /> 
          </Carousel.Item>
          <Carousel.Item>
              <img
                style={{height: "60vh"}}
                className="w-100"
                src={img3}
                alt="third slide"
              />
             {/* <Carousel.Caption className="d-flex flex-column align-items-center" style={{top: "3vh"}}>
              <div className="shadow col w-100 p-5 pt-4" style={{overflow: "hidden", height: "40vh", textAlign: "center" }}>
              <h1 style={{fontSize: "1.8rem"}} className="pb-3">"Improve your working performance more effectively and efficiently."</h1>
              <h2 style={{color: "azure"}} className="mt-5">Create a Schedule Plan now!</h2>
              <button id="btn1" type="button" onClick={() => btnRegister()} className="fw-bold fs-5 mt-2" style={{borderRadius: "30px", paddingLeft: "15px", 
                paddingRight: "15px", paddingTop: "10px", paddingBottom: "10px"}}>Get Started</button>
              </div>
              </Carousel.Caption> */}
          </Carousel.Item>
        </Carousel>
      </div>
  );
}

export default UncontrolledExample;